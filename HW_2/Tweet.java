package sayl2.sauts;

import java.util.Comparator;
import java.util.Date;

/*
*Klass sozdan dlja sortirovki. Sortirovka prohodit ispol'zuja komparator, tak kak 'to naibolee prostoj variant
*dlja sortirovki 'lementov v jave, ne nado pisat' dopolnitel'nyh algoritmov, vsjo uzhe gotovo. Primer byl vzjat so stakoverflou.
*/
public class Tweet {
	
	Date dateCreated;
	String name;
	String text;
	String orderDirection;

	public Tweet(){
	}
	
	public Tweet(Date createdAt, String name, String text) {
		super();
		this.dateCreated = createdAt;
		this.name = name;
		this.text = text;
	}
	
	public Comparator<Tweet> TweetUsernameComparator = new Comparator<Tweet>() {
		@Override
		public int compare(Tweet t1, Tweet t2) {
			String TweetName1 = t1.getName().toUpperCase();
			String TweetName2 = t2.getName().toUpperCase();
			
			if(getOrderDirection().equals("asc")){
				return TweetName1.compareTo(TweetName2);
			} else if (getOrderDirection().equals("desc")){
				return TweetName2.compareTo(TweetName1);
			}
			return 0;
		}
	};
	
	public Comparator<Tweet> TweetDateComparator = new Comparator<Tweet>() {
		@Override
		public int compare(Tweet t1, Tweet t2) {
			Date TweetDate1 = t1.getDateCreated();
			Date TweetDate2 = t2.getDateCreated();
			
			if(getOrderDirection().equals("asc")){
				return TweetDate1.compareTo(TweetDate2);
			} else if (getOrderDirection().equals("desc")){
				return TweetDate2.compareTo(TweetDate1);
			}
			return 0;
		}
	};
	
	public Comparator<Tweet> TweetTextComparator = new Comparator<Tweet>() {
		@Override
		public int compare(Tweet t1, Tweet t2) {
			String TweetText1 = t1.getName().toUpperCase();
			String TweetText2 = t2.getName().toUpperCase();
			
			if(getOrderDirection().equals("asc")){
				return TweetText1.compareTo(TweetText2);
			} else if (getOrderDirection().equals("desc")){
				return TweetText2.compareTo(TweetText1);
			} 
			return 0;
		}
	};
	
	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	public String getOrderDirection() {
		return orderDirection;
	}

	public void setOrderDirection(String orderDirection) {
		this.orderDirection = orderDirection;
	}

	@Override
	public String toString() {
		return "Tweet [dateCreated=" + dateCreated + ", name=" + name + ", text="
				+ text + "]";
	}

}
