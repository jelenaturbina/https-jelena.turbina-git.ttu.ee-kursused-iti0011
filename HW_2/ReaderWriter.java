package sayl2.sauts;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ReaderWriter {
	
	double lat;
	double lon;
    double radius;
	String location;
	List<String> allData = new ArrayList<String>();
	List<String> allLocations = new ArrayList<String>();

    public ReaderWriter(){
        readAllBufferedData();
    }

    /**
    * Chitaem dannye s fajla kohad.csv i dobavljaem ih v list AllData.
    */
    public void readAllBufferedData() {

        try {
            String fileName = "src/kohad.CSV";
            File file = new File(fileName);
            if(!file.exists()){
                file.createNewFile();
            }
            BufferedReader br = new BufferedReader( new FileReader(fileName));

            while(!((fileName = br.readLine()) == null)) {
                String[] result2 = fileName.split("/n"); // Berjom vse novye strochki

                for (int x=0; !(x >= result2.length); x++) {
                    this.allData.add(result2[x]);
                }
            }
            br.close();
            System.out.println("Done reading from kohad.CSV");
        }

        catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metod pishet dannye v fajl kohad.csv. Metod vzjat s konspekta.
     */
    public void writeBufferedDataToFile() {
        try {
            String fileName = "src/kohad.CSV";
            File file = new File(fileName);
            //Если файла нету, то создаём
            if(!file.exists()){
                file.createNewFile();
            }

            FileWriter fileWritter = new FileWriter(fileName,true);
            BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
            bufferWritter.write(getLocation() + "," + getLat() + "," + getLon() + "," + getRadius());
            bufferWritter.newLine();
            bufferWritter.close();
            System.out.println("Done writing to kohad.CSV");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*GETTERS*/
    public double getRadius() {
        return this.radius;
    }

    public List<String> getAllData() {
        return allData;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public String getLocation() {
        return location;
    }

    public List<String> getAllLocations() {
        return allLocations;
    }

    /*SETTERS*/
    public void setRadius(double radius) {
        this.radius = radius;
    }

    public void setAllData(List<String> allData) {
        this.allData = allData;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}
