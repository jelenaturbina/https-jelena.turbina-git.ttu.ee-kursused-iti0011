package sayl2.sauts;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import twitter4j.GeoLocation;
import twitter4j.Query;
import twitter4j.Query.Unit;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.OAuth2Token;
import twitter4j.conf.ConfigurationBuilder;

public class Sauts {

    // Konstanty dlja patterna
    // Patterny, po tutorialu s Interneta.
    // http://stackoverflow.com/questions/4962176/java-extract-part-of-a-string-between-two-special-characters
	private static final Pattern PATTERN_LATTITUDE = Pattern
			.compile("lat=\'(.*?)\'");
	private static final Pattern PATTERN_LONGITUDE = Pattern
			.compile("lon=\'(.*?)\'");
	private static final Pattern PATTERN_BOUNDINGBOX = Pattern
			.compile("boundingbox=\"(.*?)\"");

    // Konstanty, kljuchi dlja avtorizacii v Tvittere, ispol'zuja svoj akkaunt
    private static final String TWITTER_CUSTOMER_KEY = "eQS2whKZGoex538q89vujROdg";
    private static final String TWITTER_CUSTOMER_SECRET = "2Z9QYbLsLhlatozLemxCvhhKHa77isTy1F8I67fdbche5Bzg91";

    // Pachka peremennyh
	public LocationData locationData;
	static Input setInput;
	static ReaderWriter readerWriter = new ReaderWriter();
	public static double radius;

	static boolean shouldDataBeSaved = true;
	static boolean shouldLatLonRadSaved = true;
	public static double lat;
	public static double lon;
    public static Tweet collectionTweet = new Tweet();
    static String sortBy = null;

    // Konstruktor startuet, kogda Sauc inicializirovan n metode main
	public Sauts() throws IOException{
        this.locationData = new LocationData();
        try {
            queryGeographyData();
        } catch (Exception e) {
            System.out.println("No location found. Wrong Input.");
            System.exit(0);
        }
	}
	
	public static void main(String[] args) throws IOException {
		analyzeInput(args);
		Sauts sauts = new Sauts();
	}
	
	public void queryGeographyData() throws IOException {
		if(shouldLatLonRadSaved == true){
			setLocationUsingNominatimService();
			 if(isShouldDataBeSaved()==true){
				 setDataBufferData();}
		} else {
			 if(isShouldDataBeSaved()==true){
				 setDataBufferDataIfLatLonAlreadyExist();}
		}
		
		Twitter twitter = twitterConfiguration();
		OAuth2Token token;
		
		try {
			token = twitter.getOAuth2Token();
			Query query;  // sjuda mozhno vpisat chto konkretno ishesh, no mozhno i ne pisat
            if(setInput.getSearchBy() != null){
                query = new Query(setInput.getSearchBy()); //search by certain value is realized
            } else {
                query = new Query();  // sjuda mozhno vpisat chto konkretno ishesh, no mozhno i ne pisat
            }
			query.setGeoCode(new GeoLocation(locationData.getLat(), locationData.getLon()), 100.0, Unit.km);//location radius
			query.setCount(setInput.getTweetsNumber());
			QueryResult result = twitter.search(query);

            ArrayList<Tweet> arraylist = new ArrayList<Tweet>();
            for (Status tweet : result.getTweets()) {
                arraylist.add(new Tweet(tweet.getCreatedAt(), tweet.getUser().getName(), tweet.getText()));
            }

            //here we decide how to sort tweets in tweets.txt. For this we use methods in Tweets.java class and
            //this tutorial for sorting was used:  http://beginnersbook.com/2013/12/java-arraylist-of-object-sort-example-comparable-and-comparator/
            if(getSortBy().equals("author")){
                Collections.sort(arraylist, collectionTweet.TweetUsernameComparator);
            } else if (getSortBy().equals("date")) {
                Collections.sort(arraylist, collectionTweet.TweetDateComparator);
            } else if (getSortBy().equals("content")) {
                Collections.sort(arraylist, collectionTweet.TweetTextComparator);
            } else {
                Collections.sort(arraylist, collectionTweet.TweetUsernameComparator);
            }

            List<String> tweetsList = new ArrayList<String>();
            for(Tweet str: arraylist){
                tweetsList.add(str.toString());
            }
            writeToFile(tweetsList);
			 
		} catch (TwitterException e1) {
			e1.printStackTrace();
		}
	}

    /**
     * @throws NumberFormatException
     * znaja parametry boundingbox vychisjaem radius ispolzuja tutorial s  http://www.coderanch.com/t/620129/java/java/Calculating-distance-points-represented-lat
     */
    private double  evaluateRadius() throws NumberFormatException {
        double lat1 = 0;
        double lat2 = 0;
        double lon1 = 0;
        double lon2 = 0;

        String[] r = locationData.getBoundingbox().split(",");
        for (int i = 0; i < r.length; i++) {
            lat1 = Double.parseDouble(r[0]);
            lat2 = Double.parseDouble(r[1]);
            lon1 = Double.parseDouble(r[2]);
            lon2 = Double.parseDouble(r[3]);
        }

        double R = 6371; // km
        double dLat = Math.toRadians(lat1-lat2);
        double dLon = Math.toRadians(lon1-lon2);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat2)) * Math.cos(Math.toRadians(lat1)) *
                        Math.sin(dLon/2) * Math.sin(dLon/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        return (R * c)/2;
    }

    /**
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	private void setLocationUsingNominatimService() throws MalformedURLException, IOException {
		System.out.println(setInput.getLocationName());
		URL url = new URL("http://nominatim.openstreetmap.org/search?q=" + setInput.getLocationName() + "&format=xml");
		URLConnection connection = url.openConnection();
		BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String inputline;
		
		while ((inputline = in.readLine()) != null) {
			Matcher matcher = PATTERN_LATTITUDE.matcher(inputline);
			Matcher matcher2 = PATTERN_LONGITUDE.matcher(inputline);
			Matcher matcher3 = PATTERN_BOUNDINGBOX.matcher(inputline);
			
			while (matcher.find()) {
				locationData.setLat(Double.parseDouble(matcher.group(1)));
				break;
			}
			
			while (matcher2.find()) {
				locationData.setLon(Double.parseDouble(matcher2.group(1)));
				break;
			}
			
			while (matcher3.find()) {
				locationData.setBoundingbox(matcher3.group(1));
				break;
			}
		}
        radius = evaluateRadius();
	}

	public void setDataBufferData() {
		 readerWriter.setLat(locationData.getLat());
		 readerWriter.setLon(locationData.getLon());
         readerWriter.setRadius(radius);
		 readerWriter.setLocation(setInput.getLocationName());
         readerWriter.writeBufferedDataToFile();
	}
	
	public void setDataBufferDataIfLatLonAlreadyExist() {
		 readerWriter.setLat(getLat());
		 readerWriter.setLon(getLon());
         readerWriter.setRadius(radius);
		 readerWriter.setLocation(setInput.getLocationName());
	}

	/**
	 * Pishet dannye v fajl:  tweets.txt. Metod vzjan s konspekta.
     *
	 */
	public void writeToFile(List<String> tweets) {
		try {
				File file = new File("src/tweets.txt");
 
				// if file doesnt exists, then create it
				if (!file.exists()) {
					file.createNewFile();
				}
 
				FileWriter fw = new FileWriter(file.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				for (int i = 0; i < tweets.size(); i++) {
					bw.write(tweets.get(i));
					bw.newLine();
				}
				
				bw.close();
 
				System.out.println("Done writing to tweets.txt");
 
			} catch (IOException e) {
				e.printStackTrace();
			}
	}

    /**
     * @return Metod, vzjatyj s zadanija.
     * Nuzhen dlja autentifikacii v tvittere, chtoby ispol'zovat'
     * "Tvitter API" nuzhny dannye real'nogo pol'zovatelja tvittera.
     *
     */
	private Twitter twitterConfiguration() {
		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true).setApplicationOnlyAuthEnabled(true);
		cb.setOAuthConsumerKey(TWITTER_CUSTOMER_KEY).setOAuthConsumerSecret(TWITTER_CUSTOMER_SECRET);
		
		TwitterFactory tf = new TwitterFactory(cb.build());
		Twitter twitter = tf.getInstance();
		return twitter;
	}

    /**
     * @param Metod analiziruet vvod dannyh s konsoli, takzhe proverjaet, chto vvedeny kljuchevye slova (-location, -sort, -c, -w).
     *
     * Primer zaprosa: -location Tallinn -c 30 -w tweets.txt -sort asc author -search Ilves
     * @throws NumberFormatException
     */
	private static void analyzeInput(String[] args) throws NumberFormatException {
        try {

            if(args[0].equals("-location")){ // pervoe slovo dolzhno byt' -location
                String locationName = args[1]; // vtoroj parametr jeto locationName (e.g Tallinn, Tapa etc)
                int numberOfTweets = Integer.parseInt(args[3]);// tretij jeto nomer tvitov
                String fileName = args[5];// pjatyj jeto imja fajla, kuda tvity dolzhny byt' sohraneny (e.g tweets.txt)
                String searchBy = null;
                String sortOrientation = null;

                //when ReaderWriter is created firstly it reads kohad.csv file and adds data to lists
                locationName = analyzeDataFromCSV(locationName);
                checkLocationInWholeFile(locationName);
                checkSort(args, sortOrientation);
                searchBy = searchByName(args, searchBy);
                setInput = new Input(locationName, numberOfTweets, fileName, searchBy);
            }  else{
                System.out.println("Wrong input");
                System.exit(0);
            }
        } catch (Exception e) {
            System.out.println("Wrong input");
            System.exit(0);
        }
	}

    /**
    * Esli na 10-oj pozicii zaprosa est' jelement -search, to ishhem cherez tvitter4j po 11-mu slovu v zaprose
    *
    */
    private static String searchByName(String[] args, String searchBy) {
        if(args.length > 9 && args[9].equals("-search")){
            searchBy = args[10];
        }
        return searchBy;
    }

    /**
     * Esli na 7-oj pozicii zaprosa est' jelement -sort,
     * to sortiruem po orientacii sortOrientation i po jelementu sortBy
     *
     */
    private static String checkSort(String[] args, String sortOrientation) {
        if(args.length > 6 && args[6].equals("-sort")) {
            sortOrientation = args[7];
            collectionTweet.setOrderDirection(sortOrientation);
            sortBy = args[8];
        }
        return sortOrientation;
    }

    /*
    * Proverim, est' li takoj lokejshn vo vsjom fajle,
    * esli est' to sohranjat' vtoroj raz ne nado shouldDataBeSaved = false;
    *
    * */
    private static void checkLocationInWholeFile(String locationName) {
        for (int i = 0; i < readerWriter.getAllLocations().size(); i++) {
            if(readerWriter.getAllLocations().get(i).equals(locationName)){
                shouldDataBeSaved = false;
            }
        }
    }

    /*
    * Analiziruec CSV fajl, proverjaet kazhduju strochku fajla otdel'no, vkljuchaet v sebja 2 metoda.
    *
    * */
    private static String analyzeDataFromCSV(String locationName) {
        for (int i = 0; i < readerWriter.getAllData().size(); i++) {// proverim pustoj li fajl i kakie parametri vnutri nego kohad.csv
            String oneLine = readerWriter.getAllData().get(i).toString();// v kazhdom cikle berjom odnu strochku i analiziruem dannye v nej
            String[] result = oneLine.split(",");

            for (int x=0; x<result.length; x++) {
                locationName = checkAlternativeLocations(locationName, result, x);
                if (result[0].equals(locationName)) {
                    try {
                        checkLatLonShouldBeSaved(result);
                    } catch (Exception e) {
                        System.out.println("Wrong Input");
                        System.exit(0);
                    }
                }
            }
        }
        return locationName;
    }

    /*
    *  Proverjaet, est' li v CSV fajle dolgota, shirota i radius. Esli ih net, to stavim shouldLatLonRadSaved= tru;,
    *  chto oznachaet chto my dolzhny sohranit' R,D,Sh i ispol'zovat' NOMINATIM uslugu dlja ih vychislenija.
    *  V drugom sluchai dajom znachenija D i Sh v peremennye lat i lon.
    * */
    private static void checkLatLonShouldBeSaved(String[] result) {
        if(result.length == 1){ // proverim, esli dol. shir. ne nastroeny
            shouldLatLonRadSaved = true; // Esli ne nastroeny, to v budushhem soedinjaemsja s Nominatim uslugoj
        } else {
            lat = Double.parseDouble(result[1]);
            lon = Double.parseDouble(result[2]);
            radius = Double.parseDouble(result[3]);
            shouldLatLonRadSaved = false;
        }
    }

    /*
    * Berjom iz strochki vse znachenija posle radiusa (to est' znachenija, tam gde al'ternativnye imena).
    * I sravnivaem najdennoe v fajle al'ternativnoe znachenie s vvedjonnym znacheniem i esli oni sovpatajut,
    * to dajom peremenoj locationName pervoe znachenie s CSV fajla (tam obychno nazvanija gorodov)
    * i osushhestvljaem poisk po gorodu, a ne po al'ternativomu imeni.
    *
    * */
     private static String checkAlternativeLocations(String locationName, String[] result, int x) {
        if(x > 2){  // Proverim al'ternativnye imena dlja lokejshena
            if(result[x].equals(locationName)){
                locationName = result[0];
                shouldDataBeSaved = false;
            }
        }
        return locationName;
    }


	
	/*GETTERS AND SETTERS*/
	
	@SuppressWarnings("static-access")
	public boolean isShouldDataBeSaved() {
		return this.shouldDataBeSaved;
	}

	public double getLon() {
		return lon;
	}

    public String getSortBy(){
        return sortBy;
    }
	
	public double getLat() {
		return lat;
	}

}
