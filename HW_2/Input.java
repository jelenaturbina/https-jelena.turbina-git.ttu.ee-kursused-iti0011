package sayl2.sauts;

public class Input {

    String locationName;
    String fileName;
    String searchBy;
    int tweetsNumber;

    public Input(String locationName, int numberOfTweets, String fileName, String searchBy) {
        this.locationName = locationName;
        this.tweetsNumber = numberOfTweets;
        this.fileName = fileName;
        this.searchBy = searchBy;
    }

    /*GETTERS*/
    public String getLocationName() {
        return locationName;
    }

    public int getTweetsNumber() {
        return tweetsNumber;
    }

    public String getSearchBy() {
        return searchBy;
    }
}

