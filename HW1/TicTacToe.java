import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

/**
 * 
 */

/**
 * @author I
 * Cell values X(-1), O(1), empty(0)
 * 
 */
public class TicTacToe {
	/**
	 * TicTacToe board. Boolean for showing, is empty cell or not.
	 */
	private static boolean [] board;
	/**
	 *  Integers, written by user in console
	 */
	private static int input;
	/**
	 *  Winner player(1), bot(2), tie(3)
	 */
	private static int winner=0;
	/**
	 * Id`s for cells on board 
	 */
	private static int[] cornerCells = new int[]{0,2,6,8};
	private static int[] otherCells = new int[]{1,3,5,7};
	/**
	 * Is bot move right now or not
	 */
	private static boolean botMove = false;
	/**
	 * Game finished or not
	 */
	private static boolean isGameOver = false;
	/**
	 * Cells array show where is X or O
	 */
	private static int [] cells;
	
	/**
	 * Text scanner which is used in readInput().
	 * We declare it here as a separate variable
	 * in order to avoid warnings about not closing it.
	 */
	private static Scanner scanner = new Scanner(System.in);
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		showMessage("Works! Game TicTacToe started!",false);
		board = new boolean[9];	
		cells = new int[9];
		for (int k=0; k<9; k++){
			if(k>3) // before nobody can`t win
				winner = checkWin(cells);
			if(winner == 0 && k<9){
				makeMove(botMove);
				printBoard();
			}else{
				isGameOver = true;
				break;
			}
			
		}
		showMessage("\n-----------------\nGame over!",false);
		printBoard();
		switch (checkWin(cells)){
			case 1: showMessage("You win!",false);break;
			case 2: showMessage("You lose! Bot wins!",false);break;
			case 3: showMessage("Tie!",false);break;
			default:break;
		}
		
		
	}
	/**
	 * Function for making moves
	 * @param isBot - true means, Bot move right now
	 */
	private static void makeMove(boolean isBot) {
		
		while(true){
			if(isBot)
				input = botChoose();					
			else{
				showMessage("Your move: ",false);
				input = readInput()-1;
			}
			if(input>=0 && input<board.length){				
				if(!cellIsTaken(input)){
					board[input] = true;
					if(isBot) cells[input] = 1; 
					else cells[input] = -1;
					break;
				}else
					showMessage("This cell is busy! Try again!",true);
				
			} else
				showMessage("Only 1-9 are used! Try again!",true);
			
		}		
		if(isBot) botMove = false;
		else botMove = true;
		
	}
	/**
	 * Function for Bot choosing
	 */
	private static int botChoose() {
		int proBotMove = professionalBotLogic();
		if(proBotMove != -1){
			return proBotMove;
		}
		if(!board[4]) return 4; // choosing cell in the middle
		else if(!board[0] || !board[2] || !board[6] || !board[8]){ // choosing cells in the corner
			while(true){
				int cornerMove = cornerCells[new Random().nextInt(cornerCells.length)];
				if(!board[cornerMove])				
					return cornerMove;					
			}
				
		}else { // choosing other cells
			while(true){
				int otherMove = otherCells[new Random().nextInt(otherCells.length)];
				if(!board[otherMove])
					return otherMove;				
			}
		}
	}
	/**
	 * Function for Bot professional move
	 * ����������� ������ cells � ���������� ����� � botCells. � ������ ��������� ���������� 
	 * ��� ��������� �������� ��� ����������� �������� ��� ���������� ����. ����������� � ��������� ������ 
	 * �(-1 - ���� ���������) � ��������� �������� �� ���� ��� � ��������, ���� ��� ����������� ���� X(1 - ���� ������) � ���������
	 * �������� �� ��, ���� ��, �� ������ ���� ���� ����, ��� ����� �������� ��� ������. � ��� �� ���������� ��� ��������� ������. 
	 */
	private static int professionalBotLogic() {
		int [] botCells = cells;
		for(int i=0; i<botCells.length; i++){
			if(botCells[i] == 0){
				botCells[i] = 1;
				if(checkWin(botCells) == 2)
					return i;
				botCells[i] = -1;
				if(checkWin(botCells) == 1)
					return i;
				botCells[i] = 0;
			}
			
		}
		return -1;
	}
	/**
	 * Checks the board and returns which player
	 * has winning combination.
	 * @param cells The current state of the board
	 * @return The indicator of the player who has winning combination.
	 * If there are no winning combinations, 0 is returned.
	 * Winning values game not finished(0), player(1), bot(2), tie(3)
	 */
	public static int checkWin(int[] cells) {
		if (((cells[0] == -1) && ((cells[1] == -1)) //Check if player wins
				&& (cells[2] == -1)) 
				|| ((cells[3] == -1) 
						&& ((cells[4] == -1)) 
						&& (cells[5] == -1))
				|| ((cells[6] == -1) 
						&& ((cells[7] == -1)) 
						&& (cells[8] == -1)) 
				|| ((cells[0] == -1) 
						&& ((cells[3] == -1)) 
						&& (cells[6] == -1)) 
				|| ((cells[1] == -1) 
						&& ((cells[4] == -1)) 
						&& (cells[7] == -1)) 
				|| ((cells[2] == -1) 
						&& ((cells[5] == -1)) 
						&& (cells[8] == -1))
				|| ((cells[0] == -1) 
						&& ((cells[4] == -1)) 
						&& (cells[8] == -1)) 
				|| ((cells[2] == -1) 
						&& ((cells[4] == -1)) 
						&& (cells[6] == -1))) return 1;
		if (((cells[0] == 1) && ((cells[1] == 1)) // Check if bot wins
				&& (cells[2] == 1)) 
				|| ((cells[3] == 1) 
						&& ((cells[4] == 1)) 
						&& (cells[5] == 1)) 
				|| ((cells[6] == 1) 
						&& ((cells[7] == 1)) 
						&& (cells[8] == 1)) 
				|| ((cells[0] == 1) 
						&& ((cells[3] == 1)) 
						&& (cells[6] == 1))
				|| ((cells[1] == 1) 
						&& ((cells[4] == 1)) 
						&& (cells[7] == 1)) 
				|| ((cells[2] == 1) 
						&& ((cells[5] == 1)) 
						&& (cells[8] == 1))
				|| ((cells[0] == 1) 
						&& ((cells[4] == 1)) 
						&& (cells[8] == 1)) 
				|| ((cells[2] == 1) 
						&& ((cells[4] == 1)) 
						&& (cells[6] == 1))) return 2;
		
		int emptyCellsCounter = 0;
		for(int i=0; i<cells.length; i++)
			if(cells[i] == 0) emptyCellsCounter ++;
		if(emptyCellsCounter == 0) return 3;  // Check if tie
		
		return 0; // Game not finished
	}
	/**
	 * Check board for occupied cells
	 */
	private static boolean cellIsTaken(int input) {
		return board[input];
	}

	/**
	 * Reads a number from the standard input and returns it.
	 * @return Number read from the input
	 */
	public static int readInput() {
		if (scanner.hasNextInt()) {
			return scanner.nextInt(); 
		}
		scanner.nextLine();
		return 0;
	}
	/**
	 * For showing board in console
	 * @param message - some string message 
	 */
	private static void printBoard(){
		showMessage("+---+---+---+\n" 
				+ "| " + printSquare(0) + " | " 
				+ printSquare(1) + " | " 
				+ printSquare(2) + " |\n" 
				+ "+---+---+---+\n" 
				+ "| " + printSquare(3) + " | " 
				+ printSquare(4) + " | " 
				+ printSquare(5) + " |\n" 
				+ "+---+---+---+\n" 
				+ "| " + printSquare(6) + " | " 
				+ printSquare(7) + " | " 
				+ printSquare(8) + " |\n" 
				+ "+---+---+---+",false);
	}
	private static String printSquare(int cell) {
		switch (cells[cell]) {
	        case 1:  return "O";
	        case -1:  return "X";
	        default:  return " ";
		}
	}
	/**
	 * For showing some messages in console
	 * @param message - some string message 
	 */
	private static void showMessage(String message, boolean error){
		if(!error) System.out.println(message);
		else System.err.println(message);
	}
	

}
